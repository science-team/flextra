flextra (5.0-14) unstable; urgency=medium

  * Bump dependency on eccodes to >= 2.17.0-2 for gfortran-10 changes.
    Closes: #957211
  * Standards-Version: 4.5.0

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 23 Apr 2020 12:07:52 +0100

flextra (5.0-13) unstable; urgency=medium

  * Depend on gfortran | fortran-compiler, for flang builds
  * Look for f95 compier in d/rules, not gfortran
  * Change eccodes.patch to use pkg-config eccodes_f90 to find paths
  * Standards-Version: 4.4.0
  * Use debhelper-compat (= 12)
  * Rebuild against libeccodes (>= 2.13.1-2). Closes: #925684

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 16 Aug 2019 08:27:27 +0100

flextra (5.0-12) unstable; urgency=medium

  * Build with dh-fortran-mod >= 0.11 for canonical compiler path,
    building $fmoddir in d/rules
  * Standards-Version: 4.3.0

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 08 Jan 2019 09:15:08 +0000

flextra (5.0-11) unstable; urgency=medium

  * Add $fmoddir to FC include line. Closes: #917728
  * Standards-Vwrsion: 4.2.1
  * Undo hardcoding gfortran-8, transition complete

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 31 Dec 2018 07:51:54 +0000

flextra (5.0-10) unstable; urgency=medium

  * Force gfortran-8, because eccodes builds a gfortran8 mod file we need.

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 27 Jun 2018 16:12:41 +0100

flextra (5.0-9) unstable; urgency=medium

  * Standards-Version: 4.1.4; no changes required
  * Switch VCS to salsa.debian.org
  * Use secure URL for homepage
  * Add /usr/include/$(ARCH) to build line. Closes: #897507

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 03 May 2018 13:37:58 +0100

flextra (5.0-8) unstable; urgency=medium

  * Standards-Version: 4.1.0; no changes required

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 09 Sep 2017 11:09:34 +0100

flextra (5.0-7) unstable; urgency=medium

  * Standards-Version: 4.0.0; no changes required

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 29 Jul 2017 14:09:25 +0100

flextra (5.0-6) unstable; urgency=medium

  * Move to debhelper 10
  * Build against eccodes instead of grib_api

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 08 Dec 2016 12:38:02 +0000

flextra (5.0-5) unstable; urgency=medium

  * Set LC_ALL=C when sorting objs for compilation, to aid reproducibility
  * Standards-Version: 3.9.8. No changes required

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 06 Jul 2016 08:41:42 +0100

flextra (5.0-4) unstable; urgency=medium

  * Add git repo at debian-science to debian/control 
  * Drop linking against libjasper. Not used  (dynamically linked to gribapi)
  * Standards-Version:  3.9.7. No changes required

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 29 Feb 2016 14:59:55 +0000

flextra (5.0-3) unstable; urgency=medium

  * Build against grib-api for the gfortran transition. Closes: #777852.

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 09 Aug 2015 20:34:54 +0100

flextra (5.0-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix handling of the flextra alternative.  (Closes: #767820)

 -- Andreas Beckmann <anbe@debian.org>  Sat, 10 Jan 2015 17:57:51 +0100

flextra (5.0-2) unstable; urgency=medium

  * Drop -mcmodel=medium; breaks non-64bit archs. Closes: #766673.

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 24 Oct 2014 13:35:56 +0100

flextra (5.0-1) unstable; urgency=low

  * Initial release. (Closes: #693831)
  * Resubmit to NEW with updated info in debian/copyright

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 14 Oct 2014 13:46:44 +0100
